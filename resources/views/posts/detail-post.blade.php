@extends('layouts.base')

@section('content')
	
	<h1>{{ $detail_post->title }}</h1>
	{{ $detail_post->content }}

@stop