<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts', [
	'as'=>'list-posts',
	'uses'=>'PostsController@list_post'
]);

Route::get('/posts/create', [
	'as'=>'create-post',
	'uses'=>'PostsController@create_post'
]);

Route::post('/posts/save',[
	'as'=>'save-post',
	'uses'=>'PostsController@save_post'
]);

Route::get('/posts/{slug}', [
	'as'=>'detail-post',
	'uses'=>'PostsController@detail_post'
]);