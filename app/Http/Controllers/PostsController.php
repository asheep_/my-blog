<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// initialize Model
use App\Post;

class PostsController extends Controller
{
    #Get Data List Post
    public function list_post()
    {
    	$posts = Post::get();
		$data = [];
		$data['list_posts'] = $posts;

    	return view('posts.list-post', $data);

    }

    #Get detail post
    public function detail_post($slug)
    {
    	$posts = Post::get();
    	$post = Post::where('slug', $slug)->first();
		
		if (!$post) {
			return redirect()->route('list-posts');
		}

    	$data = [];
    	$data['detail_post'] = $post;

    	return view('posts.detail-post', $data);

    }

    #Form Create Post
    public function create_post()
    {

		return view('posts.create_post');

    }

    #For saving post
    public function save_post(Request $req)
    {

    	$this->validate($req,[
    		'title'=>'required',
    		'content'=>'required'
		]);

    	$post = new Post();

    	$post->title = $req->title;
    	$post->slug = str_slug($req->title,'-');
    	$post->content = $req->content;

    	$post->save();

    	return redirect()->route('list-posts');

    }

}
